<%-- 
    Document   : header
    Created on : 13-may-2015, 12:21:21
    Author     : arcarrillo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Videoclub</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/main.css" />
        <link href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <script src="//code.jquery.com/jquery-2.1.4.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/summernote.min.js"></script>
    </head>
    <body>
        <div class="container nopadding">
            <div class="col-md-2 lateral-menu">
                <ul>
                    <li><a href='${pageContext.request.contextPath}/admin/categories'><i class="fa fa-pie-chart"></i>&nbsp;&nbsp;Categorías</a></li>
                    <li><a href='${pageContext.request.contextPath}/admin/'><i class="fa fa-pie-chart"></i>&nbsp;&nbsp;Películas</a></li>
                </ul>
            </div>
            <div class="col-md-10 col-md-offset-2 admin-container">