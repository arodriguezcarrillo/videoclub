<%-- 
    Document   : list
    Created on : 13-may-2015, 12:23:22
    Author     : arcarrillo
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../common/admin_header.jsp" />

<div class="col-md-10 col-md-offset-1">
    <ol class="breadcrumb">
        <li><a href="#">Inicio</a></li>
        <li class="active">Pel�culas</li>
    </ol>
    <h1>Listado de pel�culas</h1>
    <div class="col-md-12">
        <a class="btn btn-default pull-right" href="${pageContext.request.contextPath}/admin/film/new">
            <i class="fa fa-plus"></i>&nbsp;&nbsp;A�adir nueva pel�cula
        </a>
    </div>
    <div class="col-md-12">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Copias</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="film" items="${films}">
            <tr>
                <td>${film.id}</td>
                <td>${film.title}</td>
                <td>${film.slug}</td>
                <td><a class="btn btn-xs btn-default" href="${pageContext.request.contextPath}/admin/film/edit/${film.id}"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Editar</a></td>
            </tr>
            </c:forEach>
        </tbody>
    </table>
    </div>
</div>
<jsp:include page="../common/admin_footer.jsp" />