<%-- 
    Document   : list
    Created on : 13-may-2015, 12:23:22
    Author     : arcarrillo
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../common/admin_header.jsp" />

<div class="col-md-10 col-md-offset-1">
    <ol class="breadcrumb">
        <li><a href="#">Inicio</a></li>
        <li><a href="${pageContext.request.contextPath}/admin/">Pel�culas</a></li>
        <li class="active">Nueva pel�cula</li>
    </ol>
    <h1>Nueva pel�cula</h1>
    <div class="col-md-12">
        <form class="form" method="POST" action="${pageContext.request.contextPath}/admin/film/new">
            <div class="form-group">
              <input type="text" class="form-control" name="name" placeholder="T�tulo">
            </div>
            <div class="form-group">
              <input type="text" class="form-control" name="slug" placeholder="Slug">
            </div>
            <div class="form-group">
              <label class="control-label">Sinopsis</label>
              <textarea class="form-control" style="min-height:200px;" name="synopsis"></textarea>
            </div>
            <div class="form-group">
                <label class="control-label">Categor�a</label>
                <select name="category_id" class="form-control" >
                    <c:forEach var="category" items="${categories}">
                        <option value="${category.id}">${category.name}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-default"><i class="fa fa-save"></i>&nbsp;&nbsp;Guardar</button>
            </div>
        </form>
    </div>
</div>
<jsp:include page="../common/admin_footer.jsp" />