<%-- 
    Document   : list
    Created on : 13-may-2015, 12:23:22
    Author     : arcarrillo
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../common/admin_header.jsp" />

<div class="col-md-5 col-md-offset-1">
    <ol class="breadcrumb">
        <li><a href="#">Inicio</a></li>
        <li><a href="${pageContext.request.contextPath}/admin/categories/">Categor�as</a></li>
        <li class="active">Edici�n de categor�a</li>
    </ol>
    <h1>Nueva categor�a</h1>
    <div class="col-md-12">
        <form class="form-horizontal" method="POST" action="${pageContext.request.contextPath}/admin/category/new">
            <div class="form-group">
              <label for="inputName" class="col-sm-2 control-label">Nombre</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="name" placeholder="Nombre"
                        value="${category != null ? category.name : ""}" />
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label">Slug</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" name="slug" placeholder="Slug">
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default"><i class="fa fa-save"></i>&nbsp;&nbsp;Guardar</button>
              </div>
            </div>
        </form>
    </div>
</div>
<jsp:include page="../common/admin_footer.jsp" />