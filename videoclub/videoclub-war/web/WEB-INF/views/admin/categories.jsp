<%-- 
    Document   : list
    Created on : 13-may-2015, 12:23:22
    Author     : arcarrillo
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../common/admin_header.jsp" />

<div class="col-md-10 col-md-offset-1">
    <ol class="breadcrumb">
        <li><a href="${pageContext.request.contextPath}/admin/">Inicio</a></li>
        <li class="active">Categor�as</li>
    </ol>
    <h1>Listado de categor�as</h1>
    <div class="col-md-12">
        <a class="btn btn-default pull-right" href="${pageContext.request.contextPath}/admin/category/new">
            <i class="fa fa-plus"></i>&nbsp;&nbsp;A�adir nueva
        </a>
    </div>
    <div class="col-md-12">
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Slug</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="category" items="${categories}">
            <tr>
                <td>${category.id}</td>
                <td>${category.name}</td>
                <td>${category.slug}</td>
                <td><a class="btn btn-xs btn-default" href="${pageContext.request.contextPath}/admin/category/edit/${category.id}"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Editar</a></td>
            </tr>
            </c:forEach>
        </tbody>
    </table>
    </div>
</div>
<jsp:include page="../common/admin_footer.jsp" />