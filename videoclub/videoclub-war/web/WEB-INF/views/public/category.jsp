<%-- 
    Document   : list
    Created on : 13-may-2015, 12:23:22
    Author     : arcarrillo
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:include page="../common/header.jsp" />

    <ol class="breadcrumb">
        <li><a href="${pageContext.request.contextPath}/">Inicio</a></li>
        <li class="active">${category.name}</li>
    </ol>
<div class="col-md-8 col-md-offset-1">
    <h1>Pel�culas de ${category.name}</h1>
    <c:if test="${fn:length(films) gt 0}">
        <c:forEach var="film" items="${films}">
        <div class="film col-sm-2">
            <header>
                <h2><a href="${pageContext.request.contextPath}/public/film/${film.slug}">${film.title}</a></h2>
            </header>
            <img  class="film-image" />
            <footer>
                <span class="label label-success">Disponible</span>
                <a href="${pageContext.request.contextPath}/public/film/${film.slug}" class="btn btn-xs btn-info pull-right">Alquilar&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></a>
            </footer>
        </div>
        </c:forEach>
    </c:if>
    <c:if test="${fn:length(films) eq 0}">
        <p><b>Lo sentimos, no hay pel�culas en esta categor�a</b></p>
    </c:if>
</div>
        <div class="col-md-3 filter">
            <h2>Ver pel�culas de </h2>
            <ul>
                <c:forEach var="category" items="${categories}">
                    <li><a href="${pageContext.request.contextPath}/public/category/${category.slug}">${category.name}</a></li>
                </c:forEach>
                
            </ul>
        </div>
<jsp:include page="../common/footer.jsp" />