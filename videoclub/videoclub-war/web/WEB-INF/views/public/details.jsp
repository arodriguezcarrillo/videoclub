<%-- 
    Document   : list
    Created on : 13-may-2015, 12:23:22
    Author     : arcarrillo
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../common/header.jsp" />
    <ol class="breadcrumb">
        <li><a href="${pageContext.request.contextPath}/">Inicio</a></li>
        <li class="active">${film.title}</li>
    </ol>
<div class="col-md-8 col-md-offset-1">
    <div class="col-md-3"><img class="film-image" /></div>
    <div class="col-md-9">
        <h1>${film.title}&nbsp;&nbsp;<a href="#" class="btn btn-success">Alquilar&nbsp;<i class="fa fa-chevron-right"></i></a></h1>
        <div class="col-md-12">
            <p>${film.synopsis}</p>
        </div>
    </div>
    <div class="col-md-12">
        <i class="fa fa-tags"></i>&nbsp;&nbsp;Categor�as: <a href="${pageContext.request.contextPath}/public/category/${film.categoryid.slug}">${film.categoryid.name}</a>
    </div>
</div>
        <div class="col-md-3 filter">
            <h2>Ver pel�culas de </h2>
            <ul>
                <c:forEach var="category" items="${categories}">
                    <li><a href="${pageContext.request.contextPath}/public/category/${category.slug}">${category.name}</a></li>
                </c:forEach>
                
            </ul>
        </div>
<jsp:include page="../common/footer.jsp" />