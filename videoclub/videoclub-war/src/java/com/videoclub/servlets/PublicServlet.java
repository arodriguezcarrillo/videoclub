/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.videoclub.servlets;

import com.videoclub.annotations.ServletPath;
import com.videoclub.beans.CategoriesBean;
import com.videoclub.beans.FilmsBean;
import com.videoclub.entities.Category;
import com.videoclub.entities.Film;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;

/**
 *
 * @author arcarrillo
 */
@WebServlet(name = "PublicServlet", urlPatterns = {"/public/*"})
public class PublicServlet extends BaseServlet {

    @EJB
    FilmsBean _filmBO;
    
    @EJB
    CategoriesBean _categoriesBO;
    
    @ServletPath(Path = "/")
    public void Main() throws IOException{
        this.SetAttribute("films", this._filmBO.GetFilms());
        this.SetAttribute("categories", this._categoriesBO.GetCategories());
        this.View("WEB-INF/views/public/list.jsp");
    }
    
    @ServletPath(Path = "/film/{slug}")
    public void Details() throws IOException{
        String slug = this.GetParameter("slug");
        Film film = this._filmBO.GetFilm(slug);
        if (film != null){
            this.SetAttribute("categories", this._categoriesBO.GetCategories());
            this.SetAttribute("film", film);
            this.View("/WEB-INF/views/public/details.jsp");
        } else
            this.Redirect("/");
        
    }
    
    @ServletPath(Path = "/category/{slug}")
    public void Category() throws IOException{
        String slug = this.GetParameter("slug");
        Category cat = this._categoriesBO.GetCategory(slug);
        this.SetAttribute("categories", this._categoriesBO.GetCategories());
        this.SetAttribute("category", cat);
        this.SetAttribute("films", this._filmBO.GetFilms(cat.getId()));
        this.View("/WEB-INF/views/public/category.jsp");
    }

}
