/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.videoclub.servlets;

import com.videoclub.annotations.ServletPath;
import com.videoclub.annotations.ServletPathMethod;
import com.videoclub.beans.CategoriesBean;
import com.videoclub.beans.FilmsBean;
import com.videoclub.entities.Film;
import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.annotation.WebServlet;

/**
 *
 * @author arcarrillo
 */
@WebServlet(name = "AdminServlet", urlPatterns = {"/admin/*"})
public class AdminServlet extends BaseServlet {

    @EJB
    CategoriesBean _categoriesBO;
    
    @EJB
    FilmsBean _filmsBO;
    
    @ServletPath(Path = "/categories")
    public void Main() throws IOException{
        this.SetAttribute("categories", this._categoriesBO.GetCategories());
        this.View("/WEB-INF/views/admin/categories.jsp");
    }
    
    @ServletPath(Path = "/")
    public void Films() throws IOException{
        this.SetAttribute("films", this._filmsBO.GetFilms());
        this.View("/WEB-INF/views/admin/films.jsp");
    }
    
    @ServletPath(Path = "/film/new")
    public void NewFilm() throws IOException{
        this.SetAttribute("categories", this._categoriesBO.GetCategories());
        this.View("/WEB-INF/views/admin/newfilm.jsp");
    }
    
    @ServletPath(Path = "/film/edit/{id}")
    public void EditFilm() throws IOException{
        String idParam = this.GetParameter("id");
        Integer id = -1;
        try{
            id = Integer.parseInt(idParam);
        } catch (Exception excep){
            System.out.println("Cannot convert id");
        }
        
        if (id > 0){
            Film film = this._filmsBO.GetFilm(id);
            this.SetAttribute("categories", this._categoriesBO.GetCategories());
            this.SetAttribute("film", film);
            this.View("/WEB-INF/views/admin/editfilm.jsp");
        } else
            this.Redirect("/");
        
    }
    
    @ServletPath(Path = "/category/new")
    public void NewCategory() throws IOException{
        this.View("/WEB-INF/views/admin/newcategory.jsp");
    }
    
    @ServletPath(Path = "/category/edit/{id}")
    public void EditCategory() throws IOException{
        String idParam = this.GetParameter("id");
        Integer id = -1;
        try{
            id = Integer.parseInt(idParam);
        } catch (Exception excep){
            System.out.println("Cannot convert id");
        }
        
        if (id > 0){
            this.SetAttribute("category", this._categoriesBO.GetCategory(id));
            this.View("/WEB-INF/views/admin/editcategory.jsp");
        } else
            this.Redirect("categories");
    }
    
    @ServletPath(Path = "/category/new", Method = ServletPathMethod.POST)
    public void NewCategorySave() throws IOException{
        String name = this.GetParameter("name");
        String slug = this.GetParameter("slug");
        
        this._categoriesBO.SaveCategory(name, slug);
        this.Redirect("categories");
    }
    
    @ServletPath(Path = "/category/save", Method = ServletPathMethod.POST)
    public void EditCategorySave() throws IOException{
        Integer id = Integer.parseInt(this.GetParameter("id"));
        String name = this.GetParameter("name");
        String slug = this.GetParameter("slug");
        
        this._categoriesBO.EditCategory(id, name, slug);
        this.Redirect("category/edit/" + id.toString());
    }
    
    @ServletPath(Path = "/film/new", Method = ServletPathMethod.POST)
    public void NewFilmSave() throws IOException{
        String name = this.GetParameter("name");
        String slug = this.GetParameter("slug");
        String synopsis = this.GetParameter("synopsis");
        Integer category = Integer.parseInt(this.GetParameter("category_id"));
        
        this._filmsBO.SaveFilm(name, synopsis, slug, category);
        this.Redirect("");
    }
    
    @ServletPath(Path = "/film/save", Method = ServletPathMethod.POST)
    public void EditFilmSave() throws IOException{
        Integer id = Integer.parseInt(this.GetParameter("id"));
        String name = this.GetParameter("name");
        String slug = this.GetParameter("slug");
        String synopsis = this.GetParameter("synopsis");
        Integer category = Integer.parseInt(this.GetParameter("category_id"));
        
        this._filmsBO.EditFilm(id, name, synopsis, slug, category);
        this.Redirect("film/edit/" + id.toString());
    }
}
