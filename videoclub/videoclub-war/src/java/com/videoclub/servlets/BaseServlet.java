/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.videoclub.servlets;

import com.videoclub.annotations.ServletPath;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author arcarrillo
 */
public class BaseServlet extends HttpServlet {
    private Method _actualMethod;
    private String[] _actualURL;
    HttpServletRequest _request;
    HttpServletResponse _response;
    
    protected String GetParameter(String name){
        int position = this.ParameterPosition(name);
        
        if (position >= 0)
            return _actualURL[position];
        else{
            return this._request.getParameter(name);
        }
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.InvokeAction(request, response);
    }
    
    private void InvokeAction(HttpServletRequest request, HttpServletResponse response) throws IOException{
        String action = "";
        
        String partial = request.getRequestURI();
        partial = partial.replace(request.getContextPath(), "");
        partial = partial.replace(request.getServletPath(), "");
        
        String[] data = partial.split("/");
        this._actualURL = data;
        this._request = request;
        this._response = response;
        
        Method info = this.GetMethod(data);
        
        if (info != null){
            try {
                this._actualMethod = info;
                info.invoke(this);
            } catch (Exception ex) {
                Logger.getLogger(BaseServlet.class.getName()).log(Level.SEVERE, null, ex);
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            } 
        } else
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
    }
    
    private Method GetMethod(String[] parts){
        Method[] frontendMethods = this.GetFrontendMethods();
        Method result = null;
        
        for (Method method : frontendMethods){
            ServletPath servletPath = method.getAnnotation(ServletPath.class);
            
            if (this.Matches(parts, servletPath)){
                result = method;
            }
        }
        
        return result;
    }
    
    private Boolean Matches(String[] parts, ServletPath servletPath){
        Boolean result = true;
        String[] pathParts = servletPath.Path().split("/");
        
        if (parts.length == pathParts.length &&
            servletPath.Method().Check(this._request.getMethod())){
            Integer i = 0;
            while (i < parts.length){
                if (!IsParameter(pathParts[i]) &&
                    !parts[i].equals(pathParts[i])){
                    result = false;
                }

                i++;
            }
        } else
            result = false;
        
        return result;
    }
    
    private Boolean IsParameter(String item){
        Boolean result = false;
        
        if (item.startsWith("{") && item.endsWith("}"))
            result = true;
        
        return result;
    }
    
    private int ParameterPosition(String name){
        int position = -1;
        if (this._actualMethod != null){
            ServletPath servletPath = this._actualMethod.getAnnotation(ServletPath.class);
        
            String[] data = servletPath.Path().split("/");
            int i = 0;
            
            while (position < 0 && i < data.length){
                if (IsParameter(data[i]) &&
                    GetParameterName(data[i]).equals(name)){
                    position = i;
                }
                
                i++;
            }
        }
        
        return position;
    }
    
    private String GetParameterName(String name){
        return name.substring(1, name.length() - 1);
    }
    
    private Method[] GetFrontendMethods(){
        List<Method> result = new ArrayList<>();
        
        Method[] data = this.getClass().getMethods();
        for(Method method : data){
            if (method.isAnnotationPresent(ServletPath.class))
                result.add(method);
        }
        
        return result.toArray(new Method[result.size()]);
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    protected void View(String path) throws IOException{
        try{
            this._request.getRequestDispatcher(path).forward(this._request, this._response);
        } catch (Exception excep){
            this._response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
    
    protected void Redirect(String path){
        
        try {
            String url = "http://" + this._request.getServerName() + ":" + this._request.getServerPort() + this._request.getContextPath() + this._request.getServletPath() + "/" + path;
            this._response.sendRedirect(url);
        } catch (Exception ex) {
            Logger.getLogger(BaseServlet.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    protected void SetAttribute(String name, Object object){
        this._request.setAttribute(name, object);
    }
}
