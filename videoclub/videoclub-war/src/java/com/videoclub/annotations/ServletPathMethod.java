/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.videoclub.annotations;

/**
 *
 * @author arcarrillo
 */
public enum ServletPathMethod {
    GET("GET"), POST("POST");
    
    String _data;
    ServletPathMethod(String data){
        this._data = data;
    }
    
    public Boolean Check(String method){
        Boolean result = this._data.equals(method);
        
        return result;
    } 
}
