/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.videoclub.beans;

import com.videoclub.entities.Category;
import java.util.List;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

/**
 *
 * @author arcarrillo
 */
@Stateless
@LocalBean
public class CategoriesBean {

    @PersistenceContext(unitName = "videoclub-ejbPU")
    EntityManager _manager;
    
    private EntityManager GetCurrentManager(){
        return this._manager;
    }
    
    public List<Category> GetCategories(){
        Query query = this.GetCurrentManager().createQuery("SELECT c FROM Category c");
        
        return query.getResultList();
    }
    
    public Category GetCategory(String slug){
        Query query = this.GetCurrentManager().createQuery("SELECT c FROM Category c WHERE c.slug = :slug");
        query.setParameter("slug", slug);
        Object result = query.getSingleResult();
        
        if (result instanceof Category)
            return (Category)result;
        else
            return null;
    }
    
    public Category GetCategory(Integer id){
        Query query = this.GetCurrentManager().createQuery("SELECT c FROM Category c WHERE c.id = :id");
        query.setParameter("id", id);
        Object result = query.getSingleResult();
        
        if (result instanceof Category)
            return (Category)result;
        else
            return null;
    }
    
    public void SaveCategory(String name, String slug){
        Category catToAdd = new Category(0, name, slug);
        
        try{
            this.GetCurrentManager().persist(catToAdd);
        } catch (Exception excep){
            System.out.println(excep.getMessage());
            throw excep;
        }
    }
    
    public void EditCategory(Integer id, String name, String slug){
        Category catToAdd = this.GetCategory(id);
        catToAdd.setName(name);
        catToAdd.setSlug(slug);
        
        try{
            this.GetCurrentManager().persist(catToAdd);
        } catch (Exception excep){
            System.out.println(excep.getMessage());
            throw excep;
        }
    }
    
}
