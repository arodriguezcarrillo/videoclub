/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.videoclub.beans;

import com.videoclub.entities.Film;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.LocalBean;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

/**
 *
 * @author alberto
 */
@Stateless
@LocalBean
public class FilmsBean {

    @EJB
    CategoriesBean _categoriesBO;
    
    @PersistenceContext(unitName = "videoclub-ejbPU")
    EntityManager _manager;
    
    private EntityManager GetCurrentManager(){
        return this._manager;
    }
    
    public List<Film> GetFilms(){
        Query query = this.GetCurrentManager().createQuery("SELECT f FROM Film f");
        
        return query.getResultList();
    }
    
    public List<Film> GetFilms(Integer catId){
        Query query = this.GetCurrentManager().createQuery("SELECT f FROM Film f WHERE f.categoryid.id = :category");
        query.setParameter("category", catId);
        
        return query.getResultList();
    }
    
    public Film GetFilm(String slug){
        Query query = this.GetCurrentManager().createQuery("SELECT f FROM Film f WHERE f.slug = :slug");
        query.setParameter("slug", slug);
        
        Object result = query.getSingleResult();
        
        if (result instanceof Film)
            return (Film)result;
        else
            return null;
    }
    
    public Film GetFilm(Integer id){
        Query query = this.GetCurrentManager().createQuery("SELECT f FROM Film f WHERE f.id = :id");
        query.setParameter("id", id);
        
        Object result = query.getSingleResult();
        
        if (result instanceof Film)
            return (Film)result;
        else
            return null;
    }
    
    public void SaveFilm(String name, String synopsis, String slug, Integer category_id){
        Film filmToAdd = new Film(0, name, synopsis, slug);
        filmToAdd.setCategoryid(this._categoriesBO.GetCategory(category_id));
        try{
            this.GetCurrentManager().persist(filmToAdd);
        } catch (Exception excep){
            System.out.println(excep.getMessage());
            throw excep;
        }
    }
    
    public void EditFilm(Integer id, String name, String synopsis, String slug, Integer category_id){
        Film filmToAdd = this.GetFilm(id);
        filmToAdd.setTitle(name);
        filmToAdd.setSynopsis(synopsis);
        filmToAdd.setSlug(slug);
        filmToAdd.setCategoryid(this._categoriesBO.GetCategory(category_id));
        try{
            this.GetCurrentManager().persist(filmToAdd);
        } catch (Exception excep){
            System.out.println(excep.getMessage());
            throw excep;
        }
    }
}
